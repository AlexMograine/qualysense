//= ../../bower_components/jquery/dist/jquery.min.js
//= ../../bower_components/owl.carousel/dist/owl.carousel.min.js
//= ../../bower_components/slick-carousel/slick/slick.min.js
;(function($){

  //= partials/circle.js
  //= partials/circle-mobile.js
  //= partials/index__slider.js
  //= partials/product/product__slider.js
  //= partials/mobile_menu.js
  //= partials/index__news-slider.js

})(jQuery);
