$(function(){

    $('.index__circle_item').hover(function(){

        var thisDataItem = $(this).attr('data-item');
        $(".index__circle_item, .index__circle_main--item").removeClass('index__circle_main--item-act');
        $('.index__circle_wrapper').find($('*[data-item="' + thisDataItem + '"]')).addClass('index__circle_main--item-act');

    });

});
