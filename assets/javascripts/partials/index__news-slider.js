$(function(){

    $('.news__carousel, .clients__carousel').owlCarousel({
        loop: true,
        margin: 10,
        items: 2,
        dots: true,
        nav: false,
        autoWidth: true,
    });

});
